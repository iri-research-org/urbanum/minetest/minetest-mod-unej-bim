
unej_bim.marker1 = {}
unej_bim.marker2 = {}
unej_bim.pos1 = {}
unej_bim.pos2 = {}
unej_bim.marker_region = {}
unej_bim.bim = {}
unej_bim.mode = {}
local storage = minetest.get_mod_storage()
if storage:get_string("bim_mode") ~= "" then
	unej_bim.mode = minetest.deserialize(storage:get_string("bim_mode"))
end

local function above_or_under(player, pnode)
	if player:get_player_control().sneak then
		return pnode.above
	else
		return pnode.under
	end
end

minetest.register_tool(":unej_bim:tool", {
	description = "Bim tool, Left-click to set 1st position, right-click to set 2nd",
	inventory_image = "tool.png",
	stack_max = 1, 				-- there is no need to have more than one
	liquids_pointable = true, 	-- ground with only water on can be selected as well

	on_use = function(itemstack, player, pointed_node) -- right click
		if player ~= nil and pointed_node ~= nil and pointed_node.type == "node" then
			local pname = player:get_player_name()
			unej_bim.pos1[pname] = above_or_under(player, pointed_node)
			unej_bim.tool_pos1(pname)

		elseif player ~= nil then
			-- count nodes by category
			local pname = player:get_player_name()

			local pos1 = unej_bim.pos1[pname]
			local pos2 = unej_bim.pos2[pname]

			if pos1 ~= nil and pos2 ~= nil then

				pos1, pos2 = unej_bim.sort_pos(pos1, pos2)
				if not unej_bim.mode[pname] then
					unej_bim.mode[pname] = "construction_batiment"
				end
				local catsum = {}
				local unej_ref = nil
				if unej_bim.referentiel ~= nil then
					unej_ref = unej_bim.referentiel[unej_bim.mode[pname]]
				end

				if(unej_ref ~= nil and unej_ref.properties ~= nil) then

					for k,v in ipairs(unej_ref.properties) do
						catsum[v] = 0
					end
				
					for y = pos1.y,pos2.y do
						for z = pos1.z,pos2.z do
							for x = pos1.x,pos2.x do
								local node = minetest.get_node ({x=x,y=y,z=z})
								if node ~= nil then
									for k,v in pairs(catsum) do
										for k1,v1 in pairs(unej_bim.categories[k].blocs) do
											if unej_bim.groupes[k1] then
												for _,v2 in ipairs(unej_bim.groupes[k1]) do
													if node.name == v2 then
														catsum[k] = catsum[k] + v1
													end
												end
											else
												if node.name == k1 then
													catsum[k] = catsum[k] + v1
												end
											end
										end
									end
								end
							end
						end
					end
				
					unej_bim.showform (pname , catsum)
				end
			end
		end
		return itemstack
	end,

	on_place = function(itemstack, player, pointed_node) -- Left Click
		if player ~= nil and pointed_node ~= nil and pointed_node.type == "node" then
			local pname = player:get_player_name()
			unej_bim.pos2[pname] = above_or_under(player, pointed_node)
			unej_bim.tool_pos2(pname)
		end
		return itemstack
	end,
})

unej_bim.showform = function (pname , catsum)
	local player = minetest.get_player_by_name (pname)

	if player ~= nil then
		if not unej_bim.mode[pname] then
			unej_bim.mode[pname] = "construction_batiment"
		end
		local str = ""
		for k,v in pairs(catsum) do
				str = str .. unej_bim.categories[k].name .. "," .. v .. ","
			end

		minetest.show_formspec(pname,
			"unej_bim:form",
			"size[16,11]"..
			"no_prepend[]"..
			"real_coordinates[false]"..
			"bgcolor[#323232FF]"..
			"label[6,1;"..unej_bim.referentiel[unej_bim.mode[pname]].name.."]" ..
			--"hypertext[1,1;14,5;name;<big>text</big>]"..
			-- "image[7,1;3,3;bim.png]"..
			"textlist[1,3;14,5;Resultat;"..str.."]"..
			"tableoptions[border=true;background=#484848]"..
			"tablecolumns[text,width=35;text,width=16]"..
			"table[1,3;14,1;BIM;Catégorie,Note]"..
			--"label[1,3;Catégorie]" ..
			--"label[9,3;Note]" ..
			"tableoptions[background=#323232;border=true]"..
			"tablecolumns[text,width=35;text,width=16]"..
			"table[1,4;14,6;BIM;" .. str .. "]"..

			"button_exit[13,10;2,1;exit;Fin]")

	end

	return true, ""
end

--marks tool position 1
unej_bim.tool_pos1 = function(name)
	local pos1, pos2 = unej_bim.pos1[name], unej_bim.pos2[name]

	if pos1 ~= nil then
		--make area stay loaded
		local manip = minetest.get_voxel_manip()
		manip:read_from_map(pos1, pos1)
	end
	if unej_bim.marker1[name] ~= nil then --marker already exists
		unej_bim.marker1[name]:remove() --remove marker
		unej_bim.marker1[name] = nil
	end
	if pos1 ~= nil then
		--add marker
		unej_bim.marker1[name] = minetest.add_entity(pos1, "unej_bim:pos1")
		if unej_bim.marker1[name] ~= nil then
			unej_bim.marker1[name]:get_luaentity().player_name = name
		end
	end
	unej_bim.mark_region(name)
end

--marks worldedit region position 2
unej_bim.tool_pos2 = function(name)
	local pos1, pos2 = unej_bim.pos1[name], unej_bim.pos2[name]

	if pos2 ~= nil then
		--make area stay loaded
		local manip = minetest.get_voxel_manip()
		manip:read_from_map(pos2, pos2)
	end
	if unej_bim.marker2[name] ~= nil then --marker already exists
		unej_bim.marker2[name]:remove() --remove marker
		unej_bim.marker2[name] = nil
	end
	if pos2 ~= nil then
		--add marker
		unej_bim.marker2[name] = minetest.add_entity(pos2, "unej_bim:pos2")
		if unej_bim.marker2[name] ~= nil then
			unej_bim.marker2[name]:get_luaentity().player_name = name
		end
	end
	unej_bim.mark_region(name)
end

unej_bim.mark_region = function(name)
	local pos1, pos2 = unej_bim.pos1[name], unej_bim.pos2[name]

	if unej_bim.marker_region[name] ~= nil then --marker already exists
		--wip: make the area stay loaded somehow
		for _, entity in ipairs(unej_bim.marker_region[name]) do
			entity:remove()
		end
		unej_bim.marker_region[name] = nil
	end

	if pos1 ~= nil and pos2 ~= nil then
		local pos1, pos2 = unej_bim.sort_pos(pos1, pos2)

		local vec = vector.subtract(pos2, pos1)
		local maxside = math.max(vec.x, math.max(vec.y, vec.z))
		local limit = tonumber(minetest.setting_get("active_object_send_range_blocks")) * 16
		if maxside > limit * 1.5 then
			-- The client likely won't be able to see the plane markers as intended anyway,
			-- thus don't place them and also don't load the area into memory
			return
		end

		local thickness = 0.2
		local sizex, sizey, sizez = (1 + pos2.x - pos1.x) / 2, (1 + pos2.y - pos1.y) / 2, (1 + pos2.z - pos1.z) / 2

		--make area stay loaded
		local manip = minetest.get_voxel_manip()
		manip:read_from_map(pos1, pos2)

		local markers = {}

		--XY plane markers
		for _, z in ipairs({pos1.z - 0.5, pos2.z + 0.5}) do
			local marker = minetest.add_entity({x=pos1.x + sizex - 0.5, y=pos1.y + sizey - 0.5, z=z}, "unej_bim:region_cube")
			if marker ~= nil then
				marker:set_properties({
					visual_size={x=sizex * 2, y=sizey * 2},
					collisionbox = {-sizex, -sizey, -thickness, sizex, sizey, thickness},
				})
				marker:get_luaentity().player_name = name
				table.insert(markers, marker)
			end
		end

		--YZ plane markers
		for _, x in ipairs({pos1.x - 0.5, pos2.x + 0.5}) do
			local marker = minetest.add_entity({x=x, y=pos1.y + sizey - 0.5, z=pos1.z + sizez - 0.5}, "unej_bim:region_cube")
			if marker ~= nil then
				marker:set_properties({
					visual_size={x=sizez * 2, y=sizey * 2},
					collisionbox = {-thickness, -sizey, -sizez, thickness, sizey, sizez},
				})
				marker:setyaw(math.pi / 2)
				marker:get_luaentity().player_name = name
				table.insert(markers, marker)
			end
		end

		unej_bim.marker_region[name] = markers
	end
end

minetest.register_entity(":unej_bim:pos1", {
	initial_properties = {
		visual = "cube",
		visual_size = {x=1.1, y=1.1},
		textures = {"pos1.png", "pos1.png",
			"pos1.png", "pos1.png",
			"pos1.png", "pos1.png"},
		collisionbox = {-0.55, -0.55, -0.55, 0.55, 0.55, 0.55},
		physical = false,
	},
	on_step = function(self, dtime)
		if unej_bim.marker1[self.player_name] == nil then
			self.object:remove()
		end
	end,
	on_punch = function(self, hitter)
		self.object:remove()
		unej_bim.marker1[self.player_name] = nil
	end,
	on_rightclick = function(self, clicker)
		print ("on right click pos1")
	end,
})

minetest.register_entity(":unej_bim:pos2", {
	initial_properties = {
		visual = "cube",
		visual_size = {x=1.1, y=1.1},
		textures = {"pos2.png", "pos2.png",
			"pos2.png", "pos2.png",
			"pos2.png", "pos2.png"},
		collisionbox = {-0.55, -0.55, -0.55, 0.55, 0.55, 0.55},
		physical = false,
	},
	on_step = function(self, dtime)
		if unej_bim.marker2[self.player_name] == nil then
			self.object:remove()
		end
	end,
	on_punch = function(self, hitter)
		self.object:remove()
		unej_bim.marker2[self.player_name] = nil
	end,
})

minetest.register_entity(":unej_bim:region_cube", {
	initial_properties = {
		visual = "upright_sprite",
		visual_size = {x=1.1, y=1.1},
		textures = {"cube.png"},
		visual_size = {x=10, y=10},
		physical = false,
	},
	on_step = function(self, dtime)
		if unej_bim.marker_region[self.player_name] == nil then
			self.object:remove()
			return
		end
	end,
	on_punch = function(self, hitter)
		local markers = unej_bim.marker_region[self.player_name]

		if not markers then
			return
		end
		for _, entity in ipairs(markers) do
			entity:remove()
		end
		unej_bim.marker_region[self.player_name] = nil
	end,
})

--- Copies and modifies positions `pos1` and `pos2` so that each component of
-- `pos1` is less than or equal to the corresponding component of `pos2`.
-- Returns the new positions.
function unej_bim.sort_pos(pos1, pos2)
	pos1 = {x=pos1.x, y=pos1.y, z=pos1.z}
	pos2 = {x=pos2.x, y=pos2.y, z=pos2.z}
	if pos1.x > pos2.x then
		pos2.x, pos1.x = pos1.x, pos2.x
	end
	if pos1.y > pos2.y then
		pos2.y, pos1.y = pos1.y, pos2.y
	end
	if pos1.z > pos2.z then
		pos2.z, pos1.z = pos1.z, pos2.z
	end
	return pos1, pos2
end

minetest.register_chatcommand("bim_mode", {
	params = "<mode>",
	description = "Change the bim mode.",
	privs = {unej_admin=true},
	func = function(name, param)
		str = ""
		for k,v in pairs(unej_bim.referentiel) do
			str = str .. "\n["..v.name.."] :"
			for _,alias in ipairs(v.aliases) do
				str = str .. alias .. ", "
				if param == alias then
					unej_bim.mode[name] = k
					minetest.chat_send_player(name, "The bim mode is now "..v.name)
					storage:set_string("bim_mode", minetest.serialize(unej_bim.mode))
					return
				end
			end
		end
		minetest.chat_send_player(name, "Your parameter don't match with any mode, as a reminder, here is the aliases list : \n"..str)
	end
})
