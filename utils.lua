unej_bim.table_unpack = function(data)
	if type(data) ~= "table" then
		return tostring(data)
	end
	local str = "{"
	local i = true
	for k,v in pairs(data) do
		if i then
			str = str .. "\""..k.."\":"..unej_bim.table_unpack(v)
			i=false
		else
			str = str .. ", \""..k.."\":"..unej_bim.table_unpack(v)
		end
	end
	str = str .. "}"
	return str
end

unej_bim.open_file = function(path)
	local file = io.open(path,"r")
	if not file then
		minetest.log("error","unej_bim -!- Error loading "..path.."\nmake sure this file exists and restart the server.")
		return false
	end
	local content = file:read("*all")
	file:close()
	local data = minetest.parse_json(content)
	if not data then
		minetest.log("error","unej_bim -!- Failed to convert json to table "..path.."\nCheck this file is in json format and restart the server.")
		return false
	end
	return data
end

unej_bim.open_file_create = function(path, default_content)
	local content = default_content
	local file = io.open(path,"r")
	if not file then
		unej_bim.write_file_raw(path, content)
	else
		content = file:read("*all")
		file:close()
	end
	local data = minetest.parse_json(content)
	if not data then
		minetest.log("error","unej_bim -!- Failed to convert json to table "..path.."\nCheck this file is in json format and restart the server.")
		return false
	end
	return data
end

unej_bim.write_file_raw = function(path, content)
	local file = io.open(path,"w+")
	if not file then
		return false
	end
	file:write(content)
	file:close()
	return true
end


unej_bim.write_file = function(path,data)
	return unej_bim.write_file_raw(minetest.write_json(data,true))
end
