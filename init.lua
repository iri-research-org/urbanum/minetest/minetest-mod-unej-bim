unej_bim = {}
unej_bim.conf_path = minetest.get_worldpath() .. "/unej_bim"

dofile(minetest.get_modpath("unej_bim") .. "/utils.lua")

minetest.mkdir(unej_bim.conf_path)

unej_bim.categories = unej_bim.open_file_create(unej_bim.conf_path .. "/properties.conf", "{}")
unej_bim.referentiel = unej_bim.open_file_create(unej_bim.conf_path.. "/referentiels.conf", "{}")
unej_bim.groupes = unej_bim.open_file_create(unej_bim.conf_path .. "/groupes.conf", "{}")

dofile(minetest.get_modpath("unej_bim") .. "/tool.lua")
