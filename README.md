# Unej Bim

This mods count the number of each mod in a specific area and make the sum of there properties.

## Usage
1) Take the unej_bim tool
2) Create an area by clicking left to put marker 1 and clicking right to put marker 2
2) Click left on marker 1 or 2 to displayed information about the area.

## Configuration
__In "unej_bim.conf_path" folder__
We can choose the conf_path in init.lua

  * `properties.conf` : the list of properties, in each properties there is the displayed name and the different blocs with their value.
  We can also put groupe of bloc like "glass".
  * `groupes.conf` is the file to manage groups of blocs like  `"glass":{"default:glass","default:obsidian_glass"}`
  * `referentiel.conf` is the file to manage groups of properties.
  Only one group will be displayed at a time.



## Commands
  - `/bim_mode <mode>` : Change the mode where check properties.
  Groups are manage into "referentiel.conf" :
  "name" is the displayed name,
  "aliases" are different aliases to use in parameter of this command
  "properties" are the properties sub this referentiel.
